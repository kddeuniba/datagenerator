package controllers;

import static spark.Spark.*;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.kdde.dataGenerator.PvDataGenerator;
import it.kdde.dataGenerator.model.DbType;
import it.kdde.dataGenerator.model.Place;
import it.kdde.dataGenerator.model.PlantType;
import it.kdde.dataGenerator.model.PvPlant;
import it.kdde.dataGenerator.model.Technology;
import spark.*;

public class DataGenerator {
	
	public static final ConcurrentHashMap<Long, PvDataGenerator> mapDataGenerator = new ConcurrentHashMap<>();
	
	public static String postUrl = "http://localhost:9000/datagenerator";
	
	public static int delay = 10;
	
	public static TimeUnit timeUnit = TimeUnit.SECONDS;
	
	private static 	ObjectMapper mapper = new ObjectMapper();
	
	private static String username = "sumatra";
	
	private static String passowrd = "suninspector";

	public static void main(String[] args) {
		// Uncomment this if you wan't spark to listen on a
		// port different than 4567.
		// setPort(5678);
		
		//http://localhost:4567/datagenerator/1/(41.12,%2016.8)/6/160/grid_connected/building/cdte/pvgis_classic/35/0/start
		
		staticFileLocation("/public");
		
		get(new Route("/"){

			@Override
			public Object handle(Request request, Response response) {
				
				if (request.session().attribute("logged") != null)		
					response.redirect("index.html");
				else 
					response.redirect("login.html");
				
				//response.status(200);
				return "";
			}
			
		});
		
		post(new Route("/signin"){

			@Override
			public Object handle(Request request, Response response) {
				
				String user = request.queryParams("username");
				String pwd = request.queryParams("password");
				
				if (user.equals(username) && pwd.equals(passowrd)){
					response.redirect("index.html");
					request.session().attribute("logged", true);
				}else
					response.redirect("login.html");
				
				return "";
			}
			
		});
		
		get(new Route("/getparams"){

			@Override
			public Object handle(Request request, Response response) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("postUrl", postUrl);
				map.put("delay", delay);
				map.put("timeUnit", timeUnit.toString());
				
				String value = null;
				try {
					value = mapper.writeValueAsString(map);
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return value;
			}
			
		});
		
		get(new Route("/updateparams"){

			@Override
			public Object handle(Request request, Response response) {
				response.redirect("index.html");
				response.status(200);
				return "";
			}
			
		});

		
		post(new Route("/updateparams"){

			@Override
			public Object handle(Request request, Response response) {
				
				postUrl = request.queryParams("postUrl");
				String sDelay = request.queryParams("delay");
				try {
					delay = Integer.parseInt(sDelay);
				} catch (Exception e) {
				}
				String sTimeUnit = request.queryParams("timeUnit");
				if (TimeUnit.valueOf(sTimeUnit) != null)
					timeUnit = TimeUnit.valueOf(sTimeUnit);
				
			    response.status(201); // 201 Created
			    response.redirect("index.html");
				return "";
			}
			
		});
		
		get(new Route(
				"/datagenerator/:id/:position/:peak/:area/:type/:place/:technology/:dbType/:angle/:aspectAngle/start") {

			@Override
			public Object handle(Request request, Response response) {
				

				PvPlant pvPlant = null;
				try {
					Long id = Long.parseLong(request.params("id"));
					String position = URLDecoder.decode(request.params("position"),"UTF-8");
					float peakPower = Float.parseFloat(request.params("peak"));
					float area = Float.parseFloat(request.params("area"));
					PlantType type = PlantType.valueOf(request.params("type"));
					Place place = Place.valueOf(request.params("place"));
					Technology technology = Technology.valueOf(request.params("technology"));
					DbType dbType = DbType.valueOf(request.params("dbType"));
					String angle = request.params("angle");
					String aspectAngle = request.params("aspectAngle");
					
					pvPlant = new PvPlant(id, position, peakPower, area,
							type, place, technology, angle, aspectAngle, dbType);
					
					PvDataGenerator generator = mapDataGenerator.get(pvPlant.id);
					
					if (generator == null){
						generator = new PvDataGenerator(pvPlant, postUrl);
						mapDataGenerator.put(pvPlant.id, generator);
					}
					
					if (!generator.isRunning())
						generator.start();
					
					
				} catch (Exception e) {
					response.status(500);
					return e.getLocalizedMessage();
				}
								
				return pvPlant.id + " simulated";
			}
		});
	
		get(new Route("/datagenerator/:id/stop"){

			@Override
			public Object handle(Request request, Response response) {
				
				Long id = Long.parseLong(request.params("id"));

				PvDataGenerator generator = mapDataGenerator.get(id);
				if (generator == null)
					return id + " stopped";
				
				if (generator.isRunning())
					generator.stop();
				
				return id + " stopped";
			}
			
		});
		
		get(new Route("/datagenerator/:id/reset"){

			@Override
			public Object handle(Request request, Response response) {
				Long id = Long.parseLong(request.params("id"));
				
				PvDataGenerator generator = mapDataGenerator.get(id);
				if (generator == null)
					return id + " reset";
				
				generator.stopAndWait();
				mapDataGenerator.remove(id);
				
				return id + " reset";
			}
			
		});
		
		get(new Route("/datagenerator/:id/status"){

			@Override
			public Object handle(Request request, Response response) {
				
				Long id = Long.parseLong(request.params("id"));
				
				PvDataGenerator generator = mapDataGenerator.get(id);
				if (generator == null)
					return id + "not_found";
				
				if (generator.isRunning())
					return id + " simulated";
				else
					return id + " stopped";
			}
		});
	}
}
