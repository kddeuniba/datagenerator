package controllers;

import static spark.Spark.*;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import spark.*;

public class HelloWorld {
	public static void main(String[] args) {
		
		String url = "http://localhost:9000/datagenerator";
		String encoded;
		try {
			encoded = URLEncoder.encode(url, "UTF-8");
			System.out.println(encoded);
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		get(new Route("/hello/:url") {
			@Override
			public Object handle(Request request, Response response) {
				String url = request.params("url");
				String decoded = null;
				
				try {
					decoded = URLDecoder.decode(url, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				return decoded;
			}
		});

	}
}
