package it.kdde.dataGenerator.model;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Questa classe realizza il modello di dato rappresentante ciascun elemento
 * della lista in output del metodo <i>getTimeEstimates()</i> della classe
 * <i>DailyRadiation</i>.
 * 
 * @see DailyRadiationGenerator
 * @see getTimeEstimates
 * @author Angelo La Costa
 * 
 */
public class AvgDailySolarIrradiance {

	/**
	 * Ora locale solare in cui vine effettuata la stima
	 */
	private Date time;

	/**
	 * Irradianza globale su un piano fisso (W/m2)
	 */
	private Double g;

	/**
	 * Irradianza diffusa su un piano fisso (W/m2)
	 */
	private Double gD;

	/**
	 * Irradianza globale a cielo sereno su un piano fisso (W/m2)
	 */
	private Double gC;

	/**
	 * Irradianza diffusa su un piano inseguitore 2 assi (W/m2)
	 */
	private Double a;

	/**
	 * Irradianza diffusa su un piano inseguitore 2 assi (W/m2)
	 */
	private Double aD;

	/**
	 * Irraggiamento globale a cielo sereno su un piano inseguitore 2 assi
	 * (W/m2)
	 */
	private Double aC;

	/**
	 * Profilo della temperatura media di giorno (???C)
	 */
	private Float temperature;

	public Double getA() {
		return a;
	}

	public Double getaC() {
		return aC;
	}

	public Double getaD() {
		return aD;
	}

	public Double getG() {
		return g;
	}

	public Double getgC() {
		return gC;
	}

	public Double getgD() {
		return gD;
	}

	public Date getTime() {
		return time;
	}

	public Float getTemperature() {
		return temperature;
	}

	/**
	 * @param split
	 * @throws NumberFormatException
	 */
	public AvgDailySolarIrradiance(String[] split, int year, int month, int day)
			throws NumberFormatException {

		// get hour and minute from the array
		String[] sTime = split[0].split(":");
		int hour = Integer.parseInt(sTime[0]);
		int minute = Integer.parseInt(sTime[1]);

		Locale locale = new Locale("it", "IT");
		Calendar date = Calendar.getInstance(locale);
		date.clear();
		date.set(year, month, day, hour, minute);

		time = date.getTime();

		g = Double.parseDouble(split[1]);
		gD = Double.parseDouble(split[2]);
		gC = Double.parseDouble(split[3]);
		a = Double.parseDouble(split[4]);

		try {
			aD = Double.parseDouble(split[5]);
		} catch (ArrayIndexOutOfBoundsException e) {
			// Caso in cui l'utente non desidera l'inseguimento solare
		}
		try {
			aC = Double.parseDouble(split[6]);
		} catch (ArrayIndexOutOfBoundsException e) {
			// Caso in cui l'utente non desidera l'inseguimento solare
		}
		// Se la tD = '-' allora avvaloro con null
		try {
			temperature = Float.parseFloat(split[7]);
		} catch (NumberFormatException g) {
			temperature = null;
		} catch (ArrayIndexOutOfBoundsException f) {

		}

	}

	@Override
	public String toString() {

		return "AvgDailySolarIrradiance [time=" + time.toString() + ", g=" + g
				+ ", gD=" + gD + ", gC=" + gC + ", a=" + a + ", aD=" + aD
				+ ", aC=" + aC + ", tD=" + temperature + "]";
	}
}
