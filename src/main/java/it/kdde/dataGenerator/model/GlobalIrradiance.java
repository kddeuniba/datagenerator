package it.kdde.dataGenerator.model;

import java.util.Date;


public class GlobalIrradiance {
	
	/**
	 * Ora locale solare in cui vine effettuata la stima
	 */
	private Date time;
	
	/**
	 *  Irradianza globale (W/m2)
	 */
	private Double g;
	
	private Float temperature;
	
	public GlobalIrradiance(Date t, Double g, Float temperature) {
		
		time = t;
		this.g = g;
		this.temperature = temperature;
	}
	
	public Double getG() {
		return g;
	}
	
	public Date getTime() {
		return time;
	}
	
	public Float getTemperature() {
		if (temperature == null)
			return -999f;
		return temperature;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Global Irradiation time=");
		builder.append(time);
		builder.append(", [g=");
		builder.append(g);
		builder.append(", temperature=");
		builder.append(temperature);
		builder.append("] \n");
		return builder.toString();
	}
	
	
}
	
