package it.kdde.dataGenerator.model;

import java.util.Date;

public class Measure {

	private Long id;
	
	private Date time;
	
	private Double energy;
	
	private Float temperature;

	public Measure(Long id, Date time, Double energy, Float temperature) {
		super();
		this.id = id;
		this.time = time;
		this.energy = energy;
		this.temperature = temperature;
	}	
	
	public String toPath(){
		return "/" + id + "/" + time.getTime() + "/" + energy + "/" + temperature;
	}

	@Override
	public String toString() {
		return "Measure [id=" + id + ", time=" + time + ", energy=" + energy
				+ ", temperature=" + temperature + "]";
	}
	
	
}
