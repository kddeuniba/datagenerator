package it.kdde.dataGenerator.model;

import java.util.Map;
import java.util.TreeMap;

/**
 * Questa classe enumerativa rappresenta il tipo di database di radiazione solare usato
 * @author Angelo La Costa
 *
 */
public enum DbType {
	
	pvgis_classic("PVGIS-classic") , 
	pvgis_cmsaf("PVGIS-CMSAF");
	
	/**
	 *  Tipo di Db usato (tipo String)
	 */
	private final String value;
	
	/**
	 * @param v		Database usato (tipo String)
	 */
	private DbType(String v) {
		this.value = v;
	}
	
	/**
	 * Questo metodo restituisce il tipo di database utilizzato
	 * 
	 * @return		Database usato (tipo String)
	 */
	public String getValue() {
		return value;
	}	
}
