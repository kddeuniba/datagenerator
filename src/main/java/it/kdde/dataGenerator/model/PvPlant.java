package it.kdde.dataGenerator.model;

import it.kdde.dataGenerator.Utils;

public class PvPlant {

	public Long id;
	
	public String position;
	
	private Double lat;
	
	private Double lng;
	
	public float peakPower;
	
	public float area;
	
	public PlantType type;
	
	public Place place;
	
	public Technology technology;
	
	public String angle;
	
	public String aspectAngle;
	
	public DbType dbType;
		
	public PvPlant() {
	}

	public PvPlant(Long id, String position, float peakPower, float area,
			PlantType type, Place place, Technology technology, String angle,
			String aspectAngle, DbType dbType) {
		super();
		this.id = id;
		this.position = position;
		this.peakPower = peakPower;
		this.area = area;
		this.type = type;
		this.place = place;
		this.technology = technology;
		this.angle = angle;
		this.aspectAngle = aspectAngle;
		this.dbType = dbType;
		
		Double[] coords = Utils.getLatAndLong(position);
		this.lat = coords[0];
		this.lng = coords[1];
	}
	
	
	public Double getLat() {
		return lat;
	}
	
	public Double getLng() {
		return lng;
	}
	
	
}
