package it.kdde.dataGenerator.model;

public enum Technology {
//	m_Si("monocristalline"), p_Si("polycrystalline"), a_Si("amorphous"), CIS("CIS"), CIGS("CIGS"),
//	CdTe("CdTe"), HIT("Ibride"), DSC("nanocristalline");
	monocristalline, polycrystalline, amorphous, cis, cigs, cdte, ibride, nanocristalline;
}
