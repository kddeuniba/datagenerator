package it.kdde.dataGenerator.model;

/**
 * Questa classe rappresenta i mesi dell'anno.
 * 
 * @author Angelo La Costa
 * 
 */
public enum Month {
	JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER;

	/**
	 * Questo metodo restituisce un mese dell'anno se il parametro passato e'
	 * valido, null altrimenti
	 * 
	 * @param value
	 *            String rappresentante un prefisso del mese
	 * @return
	 */
	public Month getMonth(String value) {

		String upperValue = value.toUpperCase();

		for (Month month : this.values()) {
			if (month.toString().contains(upperValue))
				return month;
		}
		return null;
	}

	/**
	 * Questo metodo restituisce una stringa costituente il nome del mese,su cui
	 * e' invocato il metodo, con solo il primo carattere maiuscolo.
	 * 
	 * @return Stringa di caratteri con solo il primo carattere maiuscolo
	 */
	public String uppercaseOnlyFirst() {

		String monthString = this.getMonth(this.name()).toString();
		monthString = monthString.toLowerCase();
		char[] stringArray = monthString.toCharArray();
		stringArray[0] = Character.toUpperCase(stringArray[0]);
		monthString = new String(stringArray);

		return monthString;
	}

	public int getNumDays() {
		if (this.equals(JANUARY))
			return 31;
		if (this.equals(FEBRUARY))
			return 28;
		if (this.equals(MARCH))
			return 31;
		if (this.equals(APRIL))
			return 30;
		if (this.equals(MAY))
			return 31;
		if (this.equals(JUNE))
			return 30;
		if (this.equals(JULY))
			return 31;
		if (this.equals(AUGUST))
			return 31;
		if (this.equals(SEPTEMBER))
			return 30;
		if (this.equals(OCTOBER))
			return 31;
		if (this.equals(NOVEMBER))
			return 30;

		return 31;
	}
}
