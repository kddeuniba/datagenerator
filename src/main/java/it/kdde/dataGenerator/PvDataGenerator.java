package it.kdde.dataGenerator;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.Stack;
import java.util.concurrent.TimeUnit;

import javax.management.Query;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.joda.time.DateTime;
import it.kdde.dataGenerator.model.Measure;
import it.kdde.dataGenerator.model.PvPlant;

import com.google.common.util.concurrent.AbstractScheduledService;

import controllers.DataGenerator;

public class PvDataGenerator extends AbstractScheduledService {

	private Simulator simulator;

	private DateTime startDate;

	private String baseUrl;
	
	public static String method = "/send";

	private Queue<Measure> data;

	
	// Create an HttpClient with the ThreadSafeClientConnManager.
	// This connection manager must be used if more than one thread will
	// be using the HttpClient.
	private static PoolingClientConnectionManager cm = new PoolingClientConnectionManager();
	static {
		cm.setMaxTotal(100);
	}

	private static HttpClient httpClient = new DefaultHttpClient(cm);

	public PvDataGenerator(PvPlant plant, String baseUrl)
			throws ClientProtocolException, IOException {
		simulator = new Simulator(plant);
		this.baseUrl = baseUrl;
		startDate = new DateTime();
		generate();
	}

	private void generate() throws ClientProtocolException, IOException {
		Random rnd = new Random();

		List<Measure> measures = simulator.produceDay(startDate.getYear(),
				startDate.getMonthOfYear(), startDate.getDayOfMonth(),
				rnd.nextBoolean());

		data = new LinkedList<>();
		data.addAll(measures);
	}

	@Override
	protected void runOneIteration() throws Exception {
				
		//DateTime currDate = new DateTime();
//		if (startDate.isAfter(currDate)) {
			//startDate = currDate;
			startDate = startDate.plusDays(1);
			generate();
//		}
		while(!data.isEmpty()){
//		if (!data.isEmpty()) {
			send(data.poll());
		}
	}

	private void send(Measure m) {
		String url = baseUrl + m.toPath() + method;
		System.out.println("sending: " + url);
		HttpGet httpGet = new HttpGet(url);
		try {
			httpClient.execute(httpGet);
			httpGet.reset();
		} catch (Exception e) {
			httpGet.abort();
			e.printStackTrace();
		}
	}

	@Override
	protected Scheduler scheduler() {
		return Scheduler.newFixedRateSchedule(0, DataGenerator.delay,DataGenerator.timeUnit);
	}

	@Override
	protected void shutDown() throws Exception {
		httpClient.getConnectionManager().shutdown();
		super.shutDown();
	}
}
