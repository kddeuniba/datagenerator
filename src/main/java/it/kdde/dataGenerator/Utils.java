package it.kdde.dataGenerator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

	// constant for kwh conversion
	public static final int SECONDS_ONE_HOUR = 3600;
	public static final int ONE_KW = 1000;
	public static final double QUARTER_HOUR = 0.25;

	/**
	 * convert a global irradiance average form Watt/(m^2*period) to
	 * Kwh/(m^2*period)
	 * 
	 * @param period
	 * @param globalIrradiance
	 * @return
	 */
	public static double fromWtoKwh(double period, double globalIrradiance) {
		double kwh = 0;

		kwh = (SECONDS_ONE_HOUR / ONE_KW) * period * globalIrradiance;

		return kwh;
	}

	public static double randomize(double min, double max) {
		return Math.random() * (max - min) + min;
	}

	public static Double[] getLatAndLong(String coordinates) {

		String regex = "(\\d+\\.?\\d+)";

		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(coordinates);

		Double[] coord = new Double[2];
		int i = 0;
		while (m.find()) {
			coord[i] = Double.parseDouble(m.group());
			i++;
		}

		return coord;
	}

}
