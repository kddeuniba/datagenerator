package it.kdde.dataGenerator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.client.ClientProtocolException;
import it.kdde.dataGenerator.helper.DailyRadiationGenerator;
import it.kdde.dataGenerator.helper.RendimentoBOS;
import it.kdde.dataGenerator.helper.RendimentoModulo;
import it.kdde.dataGenerator.model.AvgDailySolarIrradiance;
import it.kdde.dataGenerator.model.DbType;
import it.kdde.dataGenerator.model.GlobalIrradiance;
import it.kdde.dataGenerator.model.Measure;
import it.kdde.dataGenerator.model.Month;
import it.kdde.dataGenerator.model.PvPlant;

public class Simulator {

	private double kOmbre;
	private double rModulo;
	private double rBOS;
	// private float[] cloudCoverProbability;

	// constant for kOmbre
	private final double MINK = 0.95;
	private final double MAXK = 0.970000000000001;

	// constant for kwh conversion
	private final int SECONDS_ONE_HOUR = 3600;
	private final int ONE_KW = 1000;
	private final double QUARTER_HOUR = 0.25;

	private PvPlant pvPlant;

	public Simulator(PvPlant plant) {
		this.pvPlant = plant;
		kOmbre = Utils.randomize(MINK, MAXK);
		rModulo = new RendimentoModulo(pvPlant.technology)
				.getRendimentoModulo();
		rBOS = new RendimentoBOS(pvPlant.type).getPerditeBosTotale();
	}

	public List<Measure> produceDay(int year, int month, int day,
			boolean clearSky) throws ClientProtocolException, IOException {

		List<Measure> measures = new ArrayList<>();
		Month m = Month.values()[month-1];

		List<AvgDailySolarIrradiance> dayIrradiances = DailyRadiationGenerator
				.generate(pvPlant, pvPlant.dbType, year, m, day);

		List<GlobalIrradiance> globalEstimates = extractGlobalIrradiance(
				dayIrradiances, clearSky);

		// producible photovoltaic energy [kWh/quarter]
		double eKwh;
		// global irradiance average [kwh/(m^2*quarter)]
		double kwh;

		for (GlobalIrradiance a : globalEstimates) {

			kwh = fromWtoKwh(QUARTER_HOUR, a.getG());

			if (kwh == 0)
				continue;

			eKwh = kwh * kOmbre * rModulo * rBOS * pvPlant.area;

			Measure measure = new Measure(pvPlant.id, a.getTime(),
					(eKwh / ONE_KW), a.getTemperature());

			measures.add(measure);
			// count += (eKwh/1000);
		}

		return measures;
	}

	/**
	 * convert a global irradiance average form Watt/(m^2*period) to
	 * Kwh/(m^2*period)
	 * 
	 * @param period
	 * @param globalIrradiance
	 * @return
	 */
	private double fromWtoKwh(double period, double globalIrradiance) {
		double kwh = 0;

		kwh = (SECONDS_ONE_HOUR / ONE_KW) * period * globalIrradiance;

		return kwh;
	}

	private List<GlobalIrradiance> extractGlobalIrradiance(
			List<AvgDailySolarIrradiance> stime, boolean isClearSky) {

		double irradianzaGlobale;
		List<GlobalIrradiance> list = new ArrayList<GlobalIrradiance>();

		for (AvgDailySolarIrradiance a : stime) {

			if (isClearSky)
				irradianzaGlobale = a.getgC();
			else
				irradianzaGlobale = a.getG();

			list.add(new GlobalIrradiance(a.getTime(), (irradianzaGlobale), a
					.getTemperature()));
		}

		return list;
	}
}
