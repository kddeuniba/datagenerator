package it.kdde.dataGenerator.helper;

import it.kdde.dataGenerator.model.AvgDailySolarIrradiance;
import it.kdde.dataGenerator.model.DbType;
import it.kdde.dataGenerator.model.Month;
import it.kdde.dataGenerator.model.PvPlant;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

/**
 * Questa classe consente di calcolare le stime dei profili medi giornalieri di
 * irraggiamento solare tramite il metodo <i>getTimeEstimates</i>.
 * 
 * @author Angelo La Costa
 * 
 */
public class DailyRadiationGenerator {

	public static List<AvgDailySolarIrradiance> generate(PvPlant pv, DbType dbType,
			int year, Month m, int day) throws ClientProtocolException,
			IOException {
		
		List<AvgDailySolarIrradiance> estimates = new ArrayList<AvgDailySolarIrradiance>();

		HttpEntity respEntity = doPost(pv.getLat().toString(), pv.getLng()
				.toString(), dbType, m, pv.angle, pv.aspectAngle);

		if (respEntity == null)
			throw new RuntimeException("No result for parameters...");

		BufferedReader in = new BufferedReader(new InputStreamReader(
				respEntity.getContent()));

		AvgDailySolarIrradiance avgDsI;

		String line;
		while ((line = in.readLine()) != null) {
			if (!line.matches("^\\d\\d:\\d\\d.*"))
				continue;

			// System.out.println(line);
			String[] split = line.split("\\s+");
			avgDsI = new AvgDailySolarIrradiance(split, year, m.ordinal(), day);
			estimates.add(avgDsI);
		}

		return estimates;
	}

	/**
	 * Questo metodo restituisce una lista di stime delle medie giornaliere di
	 * irraggiamento solare, raggruppate per ora.
	 * <p>
	 * Il metodo elabora la risposta proveniente da una richiesta http, estrae
	 * le informazioni d'interesse e le inserisce in una lista, che verr??? poi
	 * restituita
	 * </p>
	 * 
	 * @return lista delle stime di generazione di elettricit??? solare
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws RuntimeException
	 */
	public static List<AvgDailySolarIrradiance> getTimeEstimates(String lat,
			String lon, DbType dbType, int year, Month m, int day,
			String angle, String aspectAngle) throws ClientProtocolException,
			IOException {

		List<AvgDailySolarIrradiance> estimates = new ArrayList<AvgDailySolarIrradiance>();

		HttpEntity respEntity = doPost(lat, lon, dbType, m, angle, aspectAngle);

		if (respEntity == null)
			throw new RuntimeException("No result for parameters...");

		BufferedReader in = new BufferedReader(new InputStreamReader(
				respEntity.getContent()));

		AvgDailySolarIrradiance avgDsI;

		String line;
		while ((line = in.readLine()) != null) {
			if (!line.matches("^\\d\\d:\\d\\d.*"))
				continue;

			// System.out.println(line);
			String[] split = line.split("\\s+");
			avgDsI = new AvgDailySolarIrradiance(split, year, m.ordinal(), day);
			estimates.add(avgDsI);
		}

		return estimates;
	}

	/**
	 * Questo metodo simula una richiesta http con metodo post e restituisce una
	 * entit??? racchiusa nella risposta http.
	 * <p>
	 * Alcuni dei parametri aggiunti alla richiesta http sono trasparenti
	 * all'utente della classe.
	 * </p>
	 * 
	 * @return Un entit??? Http associata alla risosta http
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	private static HttpEntity doPost(String drLatitude, String drLongitude,
			DbType dbType, Month month, String drAngle, String drAspectAngle)
			throws ClientProtocolException, IOException {

		List<org.apache.http.NameValuePair> formparams = new ArrayList<org.apache.http.NameValuePair>();

		// Average Daily Solar Irradiance
		formparams
				.add(new BasicNameValuePair("dr_database", dbType.getValue()));
		formparams.add(new BasicNameValuePair("selectmonth", month
				.uppercaseOnlyFirst()));
		// Irradiance on a fixed plane
		formparams.add(new BasicNameValuePair("DRangle", drAngle));
		formparams.add(new BasicNameValuePair("DRaspectangle", drAspectAngle));
		formparams.add(new BasicNameValuePair("global", "true"));
		formparams.add(new BasicNameValuePair("clearsky", "true"));

		// Irradiance on a 2-axis tracking plane
		formparams.add(new BasicNameValuePair("glob_twoaxis", "true"));
		formparams.add(new BasicNameValuePair("clearsky_twoaxis", "true"));
		formparams.add(new BasicNameValuePair("showtemperatures", "true"));

		// Output options
		// Show graph e show horizion non gestiti, il file output trattato in
		// hidden options

		// Hidden
		formparams.add(new BasicNameValuePair("outputformatchoice", "csv"));
		formparams.add(new BasicNameValuePair("DRlatitude", drLatitude));
		formparams.add(new BasicNameValuePair("DRlongitude", drLongitude));
		formparams.add(new BasicNameValuePair("DRmonth", month
				.uppercaseOnlyFirst()));
		formparams.add(new BasicNameValuePair("regionname", "europe"));
		formparams.add(new BasicNameValuePair("language", "en_en"));

		UrlEncodedFormEntity reqEntity = new UrlEncodedFormEntity(formparams,
				"UTF-8");

		HttpPost httppost = new HttpPost(
				"http://re.jrc.ec.europa.eu/pvgis/apps4/DRcalc.php");

		httppost.setEntity(reqEntity);

		HttpClient client = new DefaultHttpClient();
		HttpResponse response = client.execute(httppost);
		HttpEntity respEntity = response.getEntity();

		return respEntity;
	}

	// public static void main(String[] args) throws ClientProtocolException,
	// IOException, RuntimeException {
	//
	// int year = 2007;
	// int day = 27;
	//
	// List<AvgDailySolarIrradiance> dR = DailyRadiationGenerator
	// .getTimeEstimates("41.125914", "16.872113",
	// DbType.PVGIS_classic,year, Month.JUNE, day, "35", "0");
	// for (AvgDailySolarIrradiance d : dR) {
	// System.out.println(d);
	// }
	//
	// }

}
