package it.kdde.dataGenerator.helper;

import it.kdde.dataGenerator.Utils;
import it.kdde.dataGenerator.model.PlantType;


public class RendimentoBOS {
	
	private double perditeBosTotale;
	
	private double perditePotenza;
	private double perditeRiflessione;
	private double perditeMismatching;
	private double perditeDC;
	private double perditeSistAcc;
	private double perditeConversStatic;
	private double perditeSporcizia; //1% in tutto l'anno
	private double perditeEfficienzaAnnuale; //1.5% l'anno
	
	private final double MIN_P_SIST_ACC =  0.1;
	private final double MAX_P_SIST_ACC =  0.120000000000001;
	private final double MIN_P_CONV_STAT = 0.04;
	private final double MAX_P_CONV_STAT_GD = 0.1;
	private final double MAX_P_CONV_STAT_SA = 0.20000000000001;
	 
	
	public RendimentoBOS(PlantType tipoImpianto) {
		perditePotenza = 0.08;
		perditeRiflessione = 0.03;
		perditeMismatching = 0.05;
		perditeDC = 0.01;
		
		if(tipoImpianto.equals(PlantType.stand_alone)){
			perditeSistAcc = Utils.randomize(MIN_P_SIST_ACC, MAX_P_SIST_ACC);
			perditeConversStatic = Utils.randomize(MIN_P_CONV_STAT, MAX_P_CONV_STAT_SA);
		}	
		else{
			 perditeSistAcc = 0;
			 perditeConversStatic = Utils.randomize(MIN_P_CONV_STAT, MAX_P_CONV_STAT_GD);
		}
		
		perditeBosTotale = perditePotenza+perditeRiflessione+perditeMismatching+perditeDC+
		+perditeSistAcc+perditeConversStatic;
		
	}
	
	public double getPerditeBosTotale() {
		return perditeBosTotale;
	}
	
	
	
	

}
