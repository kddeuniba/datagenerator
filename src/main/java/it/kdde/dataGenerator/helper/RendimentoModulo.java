package it.kdde.dataGenerator.helper;

import it.kdde.dataGenerator.Utils;
import it.kdde.dataGenerator.model.Technology;

public class RendimentoModulo {

	private final double M_SI_MIN = 0.15;
	private final double M_SI_MAX = 0.180000000000001;
	private final double P_SI_MIN = 0.13;
	private final double P_SI_MAX = 0.150000000000001;
	private final double A_SI_MIN = 0.05;
	private final double A_SI_MAX = 0.080000000000001;
	private final double CIS_MIN = 0.075;
	private final double CIS_MAX = 0.095000000000001;
	private final double CDTE_MIN = 0.006;
	private final double CDTE_MAX = 0.009000000000001;
	private final double HIT = 0.173;
	private final double DSC = 0.005;

	private double rmodulo;

	public RendimentoModulo(Technology pvt) {

		if (pvt.equals(Technology.ibride))
			rmodulo = HIT;
		else if (pvt.equals(Technology.nanocristalline))
			rmodulo = DSC;
		else if (pvt.equals(Technology.cdte))
			rmodulo = Utils.randomize(CDTE_MIN, CDTE_MAX);
		else if (pvt.equals(Technology.cis) || pvt.equals(Technology.cigs))
			rmodulo = Utils.randomize(CIS_MIN, CIS_MAX);
		else if (pvt.equals(Technology.polycrystalline))
			rmodulo = Utils.randomize(P_SI_MIN, P_SI_MAX);
		else if (pvt.equals(Technology.monocristalline))
			rmodulo = Utils.randomize(M_SI_MIN, M_SI_MAX);
		else
			rmodulo = Utils.randomize(A_SI_MIN, A_SI_MAX);
	}

	public double getRendimentoModulo() {
		return rmodulo;
	}

}
