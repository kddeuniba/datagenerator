$(document).ready(function(){
	
	$.get('/getparams', function(data){
		var json = JSON.parse(data);
		$('#postUrl').attr('value',json.postUrl);
		$('#delay').attr('value',json.delay);
		$('#timeUnit').val(json.timeUnit);
	});
});